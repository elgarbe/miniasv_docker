ARG ROS_DISTRO=melodic

#FROM ros:${ROS_DISTRO}-desktop-full
FROM osrf/ros:melodic-desktop

ARG USERNAME=asv
ARG USERID=1000
ARG HOME=/home/asv
ENV DEBIAN_FRONTEND noninteractive
ENV ROS_MASTER_URI=http://10.10.10.21:11311

# Instalación Gazebo
RUN apt update && apt install -y --no-install-recommends \
        ros-${ROS_DISTRO}-mavros-msgs \
    && rm -rf /var/lib/apt/lists/

RUN echo 'Crear usuario para no generar archivos como root'; \
    groupadd -f -g ${USERID} ${USERNAME}; \
    useradd -g ${USERID} -u ${USERID} -d ${HOME} -ms /bin/bash ${USERNAME}; \
    usermod -aG sudo ${USERNAME} ; \
    echo '%sudo ALL=(ALL) NOPASSWD:ALL' >> /etc/sudoers
# RUN echo "source /opt/ros/melodic/setup.bash" >> ~/.bashrc

USER ${USERNAME}:${USERNAME}

WORKDIR ${HOME}
